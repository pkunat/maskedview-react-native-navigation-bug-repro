import React from 'react';
import {Button, View} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import MaskedView from '@react-native-masked-view/masked-view';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faCircleRight} from '@fortawesome/free-regular-svg-icons/faCircleRight';
import {faCircleLeft} from '@fortawesome/free-regular-svg-icons/faCircleLeft';

/* note: I replaced the gradient with a regular background to simplify the example */
const FontAwesomeIconWithGradient: FC = ({size, icon}) => {
  return (
    <View style={{width: size, height: size}}>
      <MaskedView
        style={{flex: 1, flexDirection: 'row', height: size}}
        maskElement={
          <View
            style={{
              backgroundColor: 'transparent',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <FontAwesomeIcon size={size} icon={icon} color="white" />
          </View>
        }>
        {/* LinearGradient was here */}
        <View style={{flex: 1, backgroundColor: 'blue'}} />
      </MaskedView>
    </View>
  );
};

function View1({navigation}) {
  return (
    <View style={{alignItems: 'center'}}>
      <FontAwesomeIconWithGradient icon={faCircleRight} size={100} />
      <Button
        title="Go to view2 (push)"
        onPress={() => navigation.push('view2')}
      />
    </View>
  );
}

function View2({navigation}) {
  return (
    <View style={{alignItems: 'center'}}>
      <FontAwesomeIconWithGradient icon={faCircleLeft} size={100} />
      <Button title="Back to view1 (pop)" onPress={() => navigation.pop()} />
    </View>
  );
}

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer initialRoute="view1">
      <Stack.Navigator initialRouteName="view1">
        <Stack.Screen name="view1" component={View1} />
        <Stack.Screen name="view2" component={View2} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
